# Backstage Changelog

The GitLab backstage changelog is a small website that tracks all interesting
backstage changes made by GitLab team members.

A backstage change is a change made to GitLab.com or the GitLab codebase,
without this being user facing. Such examples include (but are not limited to):
infrastructure changes, code refactoring, integrations with services for GitLab
employees (e.g. chatops), and so forth.

## Requirements

* Ruby 2.4 or newer
* Bundler

## Installation

Make sure Bundler is installed:

    gem install bundler

Next, install all dependencies:

    bundle install

You can then view a live preview of the website by running:

    rake

If you just want to build the website, run the following instead:

    rake build

## Usage

First clone the repository. Once cloned, follow the installation instructions.
Once installed, run:

    ./bin/backstage-changelog

The first time you will be prompted to enter your name and team, which will be
saved in `.config.yml`. Once done, you can enter a description of the work and a
link pointing to it (if any). The changes will be pushed to the master branch
automatically.
